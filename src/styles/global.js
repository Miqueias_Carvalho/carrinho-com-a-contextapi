import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
*{
    margin: 0;
    padding: 0;
    outline: none;
    box-sizing: border-box;
}

a{
    text-decoration: none;
    cursor: pointer;
}

button{
    cursor: pointer;
    width: 150px;
    height: 45px;
    border-radius: 5px;
    font-size: 16px;
}
body {
    background-color: burlywood;
}

`;
