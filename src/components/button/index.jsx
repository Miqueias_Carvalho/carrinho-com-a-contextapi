import { useContext } from "react";

import { CatalogueContext } from "../../providers/catalogue";

import { CartContext } from "../../providers/cart";

function Button({ item, type }) {
  const { cart, addToCart, removeFromCart } = useContext(CartContext);
  const { catalogue, addToCatalogue, removeFromCatalogue } =
    useContext(CatalogueContext);

  const text = type === "catalogue" ? "Add to cart" : "Remove from cart";

  const handleClick = () => {
    if (type === "catalogue") {
      removeFromCatalogue(item);
      addToCart(item);
    } else {
      removeFromCart(item);
      addToCatalogue(item);
    }
  };
  return <button onClick={handleClick}>{text}</button>;
}
export default Button;
