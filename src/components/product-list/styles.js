import styled from "styled-components";

export const Container = styled.div`
  width: 98%;
  max-width: 500px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 50px 0;
`;

export const List = styled.ul`
  list-style: none;
  width: 100%;
  display: flex;
  justify-content: space-evenly;

  li {
    display: flex;
    flex-direction: column;
    align-items: center;
    color: brown;
    font-size: 32px;
  }
`;
