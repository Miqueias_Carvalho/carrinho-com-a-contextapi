import GlobalStyle from "./styles/global";
import { AppContainer } from "./styles/App";
import ProductList from "./components/product-list";

function App() {
  return (
    <>
      <GlobalStyle />
      <AppContainer>
        <h2>Catalogue</h2>
        <ProductList type="catalogue" />
        <h2>Cart</h2>
        <ProductList type="cart" />
      </AppContainer>
    </>
  );
}

export default App;
